# setup

        python3 -m venv venv
        . venv/bin/activate
        pip install --editable .

# run

call `elastixq` with any template from [templates](./templates) directory:

        elastixq config.xml

# pipeline

## 1. generate and upload elasticsearch mapping

note: opensearch is based on [elasticsearch 7.10](https://aws.amazon.com/de/blogs/opensource/stepping-up-for-a-truly-open-source-elasticsearch/)

pull and run [docker file](https://www.elastic.co/guide/en/elasticsearch/reference/7.5/docker.html):

        docker run -p 9200:9200 -p 9300:9300 -e "discovery.type=single-node" elasticsearch:7.10.1

generate elasticsearch mapping:

        elastixq es-mapping.json > mapping.json

upload with curl:

        curl -XPUT "http://localhost:9200/documents" -H 'Content-Type: application/json' -d @mapping.json

or as one-liner (generate and upload)

        elastixq es-mapping.json | curl -XPUT "http://localhost:9200/documents3" -H 'Content-Type: application/json' -d @-

## 2. fill elasticsearch index with json extracted from xml file

generate xquery:

        elastixq elasticsearch.xq > test.xq

run xquery on xml-data-file:

        java -classpath resources/SaxonHE11-3J/saxon-he-11.3.jar net.sf.saxon.Query -q:test.xq -s:samples/N_sselt_Anweisung_zur_Bildung_angehender_Theologen_BdN_VI_.3rj88.0.xml > test.json

upload json:

      curl -XPUT "http://localhost:9200/documents/_doc/1" -H 'Content-Type: application/json' -d @test.json


check results:
        curl "http://localhost:9200/documents/_doc/1" > result.json

# other docs

# xquery test workflow

copy result of xquery to test.xq and use one of the following xquery processors (hint: saxon-he supports xquery3)

## saxon-HE (xquery 3 :-D)

        elastixq elasticsearch.xq > test.xq

        java -classpath resources/SaxonHE11-3J/saxon-he-11.3.jar net.sf.saxon.Query -q:samples/test.xq3 -s:samples/N_sselt_Anweisung_zur_Bildung_angehender_Theologen_BdN_VI_.3rj88.0.xml

or

        sudo apt install libsaxonhe-java
        java -classpath /usr/share/java/Saxon-HE.jar net.sf.saxon.Query -q:test.xq3 -s:N_sselt_Anweisung_zur_Bildung_angehender_Theologen_BdN_VI_.3rj88.0.xml

## xqilla

        sudo apt install xqilla
        xqilla -i N_sselt_Anweisung_zur_Bildung_angehender_Theologen_BdN_VI_.3rj88.0.xml test.xq | less

## saxonb-xquery

        sudo apt install libsaxonb-java
        saxonb-xquery -q:test.xq -s:N_sselt_Anweisung_zur_Bildung_angehender_Theologen_BdN_VI_.3rj88.0.xml
