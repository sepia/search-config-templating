
import click
import yaml
from jinja2 import Environment, FileSystemLoader


@click.command()
@click.argument('template')
def cli(template):
    """Render template to std:out, using values from data.yaml"""
    #Load data from YAML file into Python dictionary
    with open('./data.yml', 'r', encoding='utf-8') as stream:
        config = yaml.safe_load(stream)

    # render
    env = Environment(loader = FileSystemLoader('./'), trim_blocks=True, lstrip_blocks=True)
    template = env.get_template(f'./templates/{template}.tmpl')
    print(template.render(config))

