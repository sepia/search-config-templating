declare namespace tei="http://www.tei-c.org/ns/1.0";

for $e in //tei:TEI/tei:text/tei:group//tei:persName/text()
    return concat('{ "persons:"', ': ', '"', normalize-space($e), '"}'),

for $e in //tei:TEI/tei:text/tei:group//tei:term/text()
    return concat('{ "keywords:"', ': ', '"', normalize-space($e), '"}'),

for $e in //tei:TEI/tei:text/tei:group//@when/text()
    return concat('{ "dates:"', ': ', '"', normalize-space($e), '"}'),

for $e in //tei:TEI/tei:text/tei:group//tei:pubPlace/text()
    return concat('{ "places:"', ': ', '"', normalize-space($e), '"}')