from setuptools import setup

setup(
    name='elastixq',
    version='0.1.0',
    py_modules=['elastixq'],
    install_requires=[
        'click',
        'pyyaml', 
        'jinja2',
        'markupsafe'
    ],
    entry_points={
        'console_scripts': [
            'elastixq = elastixq:cli',
        ],
    },
)
